//
//  LookRepairViewController.swift
//  orderapp
//
//  Created by VSNB on 2017/9/9.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit

class LookRepairViewController : UIViewController,URLSessionDelegate,URLSessionDataDelegate, UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var table: UITableView!
    
    private var list: [Maintain_ios] = [Maintain_ios]()
    private var Error: String? = ""
    var waitController: UIAlertController? = nil
    var session: URLSession!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 60.0
        
        self.session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        waitController = COMMON_Funtion.ShowWaitMessage(self,"載入中")
        
        if(GetMashineRecord()){
            if(Error == ""){
                // 方式正確
                let operation_manual = BlockOperation(block: {
                    
                    DispatchQueue.main.async(execute: {
                        self.waitController?.dismiss(animated: true, completion: nil)
                        
                        self.table.reloadData()
                    });
                })
                
                //啟動執行緒
                let operationQueue = OperationQueue()
                operationQueue.addOperation(operation_manual)
            }
            else
            {
                // 有錯誤
                COMMON_Funtion.ShowMessage(self, Title: "錯誤", Message: "請確認網路狀態")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:LookRepairViewControllerCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LookRepairViewControllerCell
        
        cell.setInfo(list[indexPath.row])
        cell.setIsort(indexPath.row)
        
        return cell
    }
    
    @IBAction func BackClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // loading
    private func GetMashineRecord() -> Bool{
        let url = URL(string: COMMON_Funtion.TokenURL + "getRecord" )
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET";
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Accept")
        
        var IS_FINISN:Bool = false
        var IS_ERROR:Bool = false
        
        let task = session.dataTask(with: request, completionHandler: {
            (data,response,error)->Void in
            if (error != nil)
            {
                IS_ERROR = true
                IS_FINISN = true
            }
            else{
                let str:String = String(data: data!, encoding: .utf8)!
                print(str);
                let joKey:[String:AnyObject] = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:AnyObject]
                
            
                let Key: Bool = ((joKey["state"] as? NSNumber)?.boolValue)!
                if(Key){
                    let jArray:[AnyObject] = joKey["list"] as! [AnyObject]
                    for  i in 0 ..< jArray.count
                    {
                        let jo:[String:AnyObject] = jArray[i] as! [String:AnyObject]
                        let m: Maintain_ios = Maintain_ios()
                        
                        m.mt_date = jo["mt_date"] as? String
                        m.mt_desc = jo["mt_desc"] as? String
                        m.mt_machine = jo["mt_machine"] as? String
                        m.mt_store = jo["mt_store"] as? String
                        m.mt_stts = jo["mt_stts"] as? String
                        
                        self.list.append(m)
                    }
                }
                else{
                    self.Error = joKey["result"] as? String
                }
                IS_FINISN = true
            }
        })
        
        task.resume()
        while(!IS_FINISN) {
            RunLoop.current.run(until: Date())
        }
        
        return !IS_ERROR
    }
}
