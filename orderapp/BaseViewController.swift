//
//  BaseViewController.swift
//  orderapp
//
//  Created by VSNB on 2017/8/17.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var showTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        showTable.delegate = self
        showTable.dataSource = self
        showTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // add ui
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:BaseViewControllerCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BaseViewControllerCell
        cell.setButtonText(indexPath.row)
        if (indexPath.row == 0){
            cell.OnOpenClick = self.goToWrittingPage
        }
        else if (indexPath.row == 1){
            cell.OnOpenClick = self.goToReadingPage
        }
        else if (indexPath.row == 6){
            cell.OnOpenClick = self.OnLoginOutClickBack
        }
        else{
            cell.OnOpenClick = self.OnPutCarClickBack
        }
        
        cell.frame.size = CGSize(width: cell.frame.size.width, height: 61)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func OnLoginOutClickBack() -> Void{
        ShowLogoutMessage(self, Title: "登出確認", Message: "確定登出?")
        
    }
    
    func OnPutCarClickBack() -> Void {
        let vc:BaseInfoViewController? = self.storyboard!.instantiateViewController(withIdentifier: "BaseInfoViewController") as? BaseInfoViewController
        
        present(vc!, animated: true, completion: {
            vc?.setInfo(COMMON_Funtion.Isort, COMMON_Funtion.Title)
        })
    }
    
    func goToWrittingPage() -> Void {
        let vc:NewRepairViewController? = self.storyboard!.instantiateViewController(withIdentifier: "NewRepairViewController") as? NewRepairViewController
        
        present(vc!, animated: true, completion: nil)
    }
    
    func goToReadingPage() -> Void {
        let vc:LookRepairViewController? = self.storyboard!.instantiateViewController(withIdentifier: "LookRepairViewController") as? LookRepairViewController
        
        present(vc!, animated: true, completion: nil)
    }
    
    func ShowLogoutMessage(_ viewControl:UIViewController,Title: String, Message: String)
    {
        let alertController = UIAlertController(title : Title,
                                                message : Message,
                                                preferredStyle : UIAlertControllerStyle.alert)
        
        let alertAction = UIAlertAction(title : "確定",
                                        style : UIAlertActionStyle.default,
                                        handler : {(UIAlertAction)->Void in
                                            let _ = COMMON_Temporary.setLogOut()
                                            let vc:LoginViewController? = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                                            
                                            self.present(vc!, animated: true, completion: nil)
        } )
        
        let cancelAction = UIAlertAction(title : "放棄",
                                         style : UIAlertActionStyle.default,
                                         handler : nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(alertAction)
        
        viewControl.present(alertController,
                            animated : true,
                            completion : nil)
        
    }
}
