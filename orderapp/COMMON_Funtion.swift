//
//  COMMON_Funtion.swift
//  orderapp
//
//  Created by VSNB on 2017/7/27.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit

class COMMON_Funtion {
    enum ServiceModeModeType {
        case LOCAL
        case TEST
        case RELEASE
    }
    
    //設定 Service URL 連線位置
    public static let ServiceMode: ServiceModeModeType = .RELEASE
    
    public static var UDeviceString : String = "";
    public static var URL_String : String = "http://orderap.hongya88.com.tw/apstore/aps001.aspx?tkn="
//    public static var URL_String : String = "http://61.63.232.67:10080/apstore/aps001.aspx?tkn="
    // <!-- 註解不別去碰，因為會導致無法上架。 除非是要測試程式 -->
    public static var VersionURL_Local:String = "http://192.168.1.122/Hongya88/api/Values/"
    public static var VersionURL_Test:String = "http://localhost:51819/api/Values/"
    public static var VersionURL:String = "http://orderapios.hongya88.com.tw:18080/hongya88/api/Values/"    //<-- 改這一個網址
    
    public static var TokenURL: String {
        get {
            /*
             */
             if ServiceMode == .TEST {
                return VersionURL_Test
             }
             else if ServiceMode == .LOCAL{
                return VersionURL_Local
            }
            return VersionURL
        }
    }
    
    public static func getStringToBase64(str:String) -> String{
        return Data(str.utf8).base64EncodedString()
    }
    
    public static var Title:String = ""
    public static var Isort: Int = 0
    public static var Token: String = ""
    public static var IS_Login = false
    public static var Account: String? = ""
    public static var Password: String? = ""
    public static var StoreName: String? = "麥克店"
    
    class func ShowWaitMessage(_ viewControl:UIViewController,_ Message: String) -> UIAlertController {
        //create an alert controller
        let pending = UIAlertController(title: nil, message: "\n"+Message, preferredStyle: .alert)
        
        //create an activity indicator
        let indicator = UIActivityIndicatorView()
        indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        indicator.activityIndicatorViewStyle = .whiteLarge
        indicator.color = UIColor(red: 15/255, green: 116/255, blue: 155/255, alpha: 1)
        
        ////add the activity indicator as a subview of the alert controller's view
        pending.view.addSubview(indicator)
        indicator.isUserInteractionEnabled = false // required otherwise if there buttons in the UIAlertController you will not be able to press them
        indicator.isHidden = true
        
        viewControl.present(pending, animated: true, completion: {
            let fullScreenSize = pending.view.frame.size
            let rect: CGRect = CGRect(x: (fullScreenSize.width-37)/2, y: 3, width: 37, height: 37)
            indicator.frame = rect
            indicator.isHidden = false
            indicator.startAnimating()
            RunLoop.current.run(until: Date())
        })
        RunLoop.current.run(until: Date())
        return pending
    }
    
    class func ShowMessage(_ viewControl:UIViewController,Title: String, Message: String)
    {
        let alertController = UIAlertController(title : Title,
                                                message : Message,
                                                preferredStyle : UIAlertControllerStyle.alert)
        
        let alertAction = UIAlertAction(title : "OK",
                                        style : UIAlertActionStyle.default,
                                        handler : nil)
        
        alertController.addAction(alertAction)
        
        viewControl.present(alertController,
                            animated : true,
                            completion : {
                                RunLoop.current.run(until: Date())
        })
    }
}
