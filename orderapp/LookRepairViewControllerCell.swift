//
//  LookRepairViewControllerCell.swift
//  orderapp
//
//  Created by VSNB on 2017/9/9.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit


class LookRepairViewControllerCell: UITableViewCell{
    @IBOutlet weak var edDate: UILabel!
    @IBOutlet weak var edMashine: UILabel!
    @IBOutlet weak var edResult: UILabel!
    @IBOutlet weak var edState: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    public func setInfo(_ m: Maintain_ios){
        edDate.text = m.mt_date!
        edMashine.text = m.mt_machine!
        edResult.text = m.mt_desc!
        edState.text = m.mt_stts!
    }
    
    public func setIsort(_ isort: Int){
        if(isort%2 == 1){
            viewBg.backgroundColor  = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        }
    }
}
