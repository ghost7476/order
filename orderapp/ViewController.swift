//
//  ViewController.swift
//  orderapp
//
//  Created by VSNB on 2017/7/27.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import UIKit

class ViewController: UIViewController,URLSessionDelegate,URLSessionDataDelegate  {
    
    var session: URLSession!
    var timerTest : Timer!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        let operation_manual = BlockOperation(block: {
//
//            DispatchQueue.main.async(execute: {
//                while (COMMON_Funtion.Token == "" || COMMON_Temporary.getFCMToken() != nil) {
//                     Thread.sleep(forTimeInterval: 0.5)
//                    print("")
//                }
////                let vc:FormalViewController? = self.storyboard!.instantiateViewController(withIdentifier: "FormalViewController") as? FormalViewController
////
////                self.present(vc!, animated: true, completion: nil)
//            })
//        })
        
        
        //啟動執行緒
//        let operationQueue = OperationQueue()
//        operationQueue.addOperation(operation_manual)
        /*
            let config = URLSessionConfiguration.default
            config.timeoutIntervalForRequest = 10.0
            
            session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        DoQuery(Query_Callback)*/
        
        startTimer () 
    }

    func startTimer () {
        if timerTest == nil {
            timerTest =  Timer.scheduledTimer(
                timeInterval: TimeInterval(0.3),
                target      : self,
                selector    : #selector(checkToken),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    @objc func checkToken() {
        if(COMMON_Funtion.Token != "") {
            print("TOKEN CHECK OK")
            if timerTest != nil {
                timerTest.invalidate()
                timerTest = nil
                let vc:FormalViewController? = self.storyboard!.instantiateViewController(withIdentifier: "FormalViewController") as? FormalViewController
                
             self.present(vc!, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Query_Callback(_ is_success:Bool) -> Void {
        if !is_success {
            let operation_manual = BlockOperation(block: {
                
                DispatchQueue.main.async(execute: {
                    if(COMMON_Temporary.getTemporary()){
                        if(COMMON_Funtion.IS_Login){
                            let vc:BaseViewController? = self.storyboard!.instantiateViewController(withIdentifier: "BaseViewController") as? BaseViewController
                            
                            self.present(vc!, animated: true, completion: nil)
                        }
                        else{
                            let vc:LoginViewController? = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            
                            self.present(vc!, animated: true, completion: nil)
                        }
                    }
                    else{
                        let vc:LoginViewController? = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        
                        self.present(vc!, animated: true, completion: nil)
                    }
                })
            })
            
            //啟動執行緒
            let operationQueue = OperationQueue()
            operationQueue.addOperation(operation_manual)
        }
        else{
            let operation_manual = BlockOperation(block: {
                
                DispatchQueue.main.async(execute: {
                    let vc:FormalViewController? = self.storyboard!.instantiateViewController(withIdentifier: "FormalViewController") as? FormalViewController
                    
                    self.present(vc!, animated: true, completion: nil)
                    
                })
            })
            
            //啟動執行緒
            let operationQueue = OperationQueue()
            operationQueue.addOperation(operation_manual)
        }
    }

    func DoQuery(_ callBackFunction:((_ is_success:Bool)->Void)?)-> Void {
        let url = URL(string: COMMON_Funtion.TokenURL + "Get1?ver=" + LoadVer())
        var request = URLRequest(url: url!)
        request.httpMethod = "GET";
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request, completionHandler: {
            (data,response,error)->Void in
            
            if (error != nil)
            {
                if callBackFunction != nil {
                    callBackFunction!(false)
                }
            }
            else
            {
                //let str:String = String(data: data!, encoding: .utf8)!
                let joRet:[String:AnyObject] = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:AnyObject]
                let state = (joRet["state"] as? NSNumber)?.intValue
                
                COMMON_Funtion.StoreName = joRet["name"] as? String
                
                if callBackFunction != nil {
                    if(state == 1){
                        callBackFunction!(true)
                    }
                    else if(state == 0){
                        callBackFunction!(false)
                    }
                }
            }
        } )
        
        task.resume()
    }
    
    func LoadVer() -> String {
        let dictionary = Bundle.main.infoDictionary!
        //let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleShortVersionString"] as! String
        return build as String
    }
}

