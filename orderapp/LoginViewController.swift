//
//  LoginViewController.swift
//  orderapp
//
//  Created by VSNB on 2017/9/7.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit


class LoginViewController: UIViewController,URLSessionDelegate,URLSessionDataDelegate{
    
    @IBOutlet weak var edAcc: UITextField!
    @IBOutlet weak var edPwd: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnNone: UIButton!
    
    var waitController: UIAlertController? = nil
    var session: URLSession!
    private var ErrorMsg:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30.0
        
        self.session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        btnNone.isHidden = true
        if(COMMON_Funtion.Account != nil){
            edAcc.text = COMMON_Funtion.Account
        }
        
        if(COMMON_Funtion.Password != nil){
            edPwd.text = COMMON_Funtion.Password
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func LoginClick(_ sender: Any) {
        
        waitController = COMMON_Funtion.ShowWaitMessage(self,"登入中")
        
        if (DoLogin(edAcc.text!, edPwd.text!)){
            let operation_manual = BlockOperation(block: {
                
                DispatchQueue.main.async(execute: {
                    self.waitController?.dismiss(animated: true, completion: {
                        if (self.ErrorMsg != ""){
                            COMMON_Funtion.ShowMessage(self, Title: "登入失敗", Message: self.ErrorMsg)
                        }
                        else{
                            let _ = COMMON_Temporary.setTemporary(self.edAcc.text!, self.edPwd.text!)
                            let vc:BaseViewController? = self.storyboard!.instantiateViewController(withIdentifier: "BaseViewController") as? BaseViewController
                            
                            self.present(vc!, animated: true, completion: nil)
                        }
                    })
                });
            })
            
            //啟動執行緒
            let operationQueue = OperationQueue()
            operationQueue.addOperation(operation_manual)
        }
        else{
            let operation_manual = BlockOperation(block: {
                
                DispatchQueue.main.async(execute: {
                    self.waitController?.dismiss(animated: true, completion: nil)
                    
                    COMMON_Funtion.ShowMessage(self, Title: "登入失敗", Message: "請確認網路狀態")
                });
            })
            
            //啟動執行緒
            let operationQueue = OperationQueue()
            operationQueue.addOperation(operation_manual)
        }
    }
    
    
    func DoLogin(_ acc:String,_ pwd: String)-> Bool {
        let jd:[String:String] = ["acc":acc, "pwd": pwd]
        
        let data:Data? = try? JSONSerialization.data(withJSONObject: jd, options: .prettyPrinted)
        
        let url = URL(string: COMMON_Funtion.TokenURL  + "Login")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST";
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Accept")
        request.httpBody = data;
        
        var IS_FINISN:Bool = false
        var IS_ERROR:Bool = false
        
        let task = session.dataTask(with: request, completionHandler: {
            (data,response,error)->Void in
            
            if (error != nil)
            {
                IS_ERROR = true
                IS_FINISN = true
            }
            else
            {
                //let str:String = String(data: data!, encoding: .utf8)!
                let joRet:[String:AnyObject] = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:AnyObject]
                let state = (joRet["state"] as? NSNumber)?.boolValue
                if (state!){
                    self.ErrorMsg = ""
                }
                else{
                    self.ErrorMsg = (joRet["result"] as? String)!
                }
                IS_FINISN = true
            }
        })
        
        task.resume()
        while(!IS_FINISN) {
            RunLoop.current.run(until: Date())
        }
        
        return !IS_ERROR
    }
}
