//
//  COMMON_Temporary.swift
//  orderapp
//
//  Created by VSNB on 2017/9/7.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation


class COMMON_Temporary{
    public static func getTemporary() -> Bool{
        //讀取記事本
        let preferencesRead = UserDefaults.standard
        
        if preferencesRead.object(forKey: "state") == nil {
            //  Doesn't exist
            NSLog("NO state", "...")
            COMMON_Funtion.IS_Login = false
            return false
        } else {
            COMMON_Funtion.IS_Login = preferencesRead.bool(forKey: "state") //讀取布林值
            COMMON_Funtion.Account = preferencesRead.string(forKey: "acc") //讀取Double
            COMMON_Funtion.Password = preferencesRead.string(forKey: "pwd") //讀取int
            return true
        }
    }
    
    public static func setTemporary(_ acc:String, _ pwd:String) -> Bool{
        //儲存在記事本
        let preferencesSave = UserDefaults.standard
        
        preferencesSave.setValue(acc, forKey: "acc") //儲存字串
        preferencesSave.set(true, forKey: "state") //儲存布林值
        preferencesSave.set(pwd, forKey: "pwd") //儲存Double
        //儲存
        let didSave = preferencesSave.synchronize()
        
        return didSave
    }
    
    public static func setFCMToken(_ token: String) {
        let userDefault = UserDefaults.standard
        userDefault.set(token, forKey: "token")
        userDefault.synchronize()
    }
    
    public static func getFCMToken()-> String?{
        let token = UserDefaults.standard.string(forKey: "token")
        return token
    }
    
    public static func setLogOut() -> Bool{
        let preferencesSave = UserDefaults.standard
        preferencesSave.set(false, forKey: "state") //儲存布林值//儲存
        let didSave = preferencesSave.synchronize()
        
        return didSave
    }
}
