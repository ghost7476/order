//
//  BaseViewControllerCell.swift
//  orderapp
//
//  Created by VSNB on 2017/8/17.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit


class BaseViewControllerCell: UITableViewCell {
//    private var showText:[String] = ["最近訂單", "查詢訂單", "快訊", "門市資訊", "數位學習", "服務品質知識庫"]
    private var showText:[String] = ["新增報修", "查詢報修", "快訊", "門市資訊", "數位學習", "服務品質知識庫", "登出"]
    
    private let ShowInfo:[String] = ["", "", "SGS食品檢驗報告-弘爺綠茶", "<中區是會議>順安店", "<店家經營管理實務>仁化店", "如何快速製作早餐-朱儀倩店長", ""]
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbInfo: UILabel!
    private var num: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func setShowMessage(_ txt: String){
        lbInfo.isHidden = false
        lbInfo.text = txt
    }
    
    public func setButtonText(_ isort: Int) {
        lbTitle.text = showText[isort]
        setShowMessage(ShowInfo[isort])
        num = isort;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func onEnterClick(_ sender: Any) {
        COMMON_Funtion.Title = lbTitle.text!
        COMMON_Funtion.Isort = num
        OnOpenClick?()
    }
    // click Event
    var OnOpenClick: (() -> Void)?
}
