//
//  NewRepairViewController.swift
//  orderapp
//
//  Created by VSNB on 2017/9/7.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit


class NewRepairViewController: UIViewController,URLSessionDelegate,URLSessionDataDelegate, UITextFieldDelegate, UIPickerViewDataSource,UIPickerViewDelegate{

    @IBOutlet weak var edStoreName: UILabel!
    @IBOutlet weak var edmachine: UITextField!
    @IBOutlet weak var edResult: UITextView!
    @IBOutlet weak var pickerDate: UIDatePicker!
    @IBOutlet weak var btnEnter: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    private var waitController: UIAlertController? = nil
    private var session: URLSession!
    private var ErrorMsg: String = ""
    private var InThread: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edResult.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1.0).cgColor
        edResult.layer.borderWidth = 1.0
        edStoreName.text = COMMON_Funtion.StoreName
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 60.0
        
        self.session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        getDate_Text(dateFormatter)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDate_Text(_ dateFormatter:DateFormatter) -> Void{
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        let date2 = String(year) + "-" + String(month) + "-" + String(day)
        
        // 可以選擇的最晚日期時間
        let endDateTime = dateFormatter.date(from: date2)
        
        // 設置可以選擇的最晚日期時間
        pickerDate.maximumDate = endDateTime
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "";
    }
    
    @IBAction func EnterClick(_ sender: Any) {
        if((edmachine.text?.characters.count)! <= 0){
            COMMON_Funtion.ShowMessage(self, Title: "錯誤", Message: "請輸入報修設備")
            return
        }
        else if((edResult.text?.characters.count)! <= 0){
            COMMON_Funtion.ShowMessage(self, Title: "錯誤", Message: "請輸入故障原因")
            return
        }
        waitController = COMMON_Funtion.ShowWaitMessage(self,"傳送中")
        if(SetMashineRecord(edmachine.text!, edResult.text!)){
            let operation_manual = BlockOperation(block: {
                DispatchQueue.main.async(execute: {
                    self.waitController?.dismiss(animated: true, completion: {
                        if (self.ErrorMsg != ""){
                            COMMON_Funtion.ShowMessage(self, Title: "錯誤", Message: self.ErrorMsg)
                        }
                        else{
                            COMMON_Funtion.ShowMessage(self, Title: "成功", Message: "維修資訊傳送成功")
                        }
                    })
                    
                    self.edmachine.text = ""
                    self.edResult.text = ""
                    
                });
            })
            
            //啟動執行緒
            let operationQueue = OperationQueue()
            operationQueue.addOperation(operation_manual)
        }
        else{
            COMMON_Funtion.ShowMessage(self, Title: "錯誤", Message: "請確認網路狀態")
        }
    }
    
    @IBAction func CancelClick(_ sender: Any) {
        edmachine.text = ""
        edResult.text = ""
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func SetMashineRecord(_ ma: String?, _ res: String?) -> Bool {
        let jd:[String:String] = [
            "store": COMMON_Funtion.getStringToBase64(str: COMMON_Funtion.StoreName!),
            "date": COMMON_Funtion.getStringToBase64(str: self.getDate()),
            "machine": COMMON_Funtion.getStringToBase64(str: ma!),
            "desc": COMMON_Funtion.getStringToBase64(str: res!)
        ]
        
        let data:Data? = try? JSONSerialization.data(withJSONObject: jd, options: .prettyPrinted)
        
        let url = URL(string: COMMON_Funtion.TokenURL  + "setSend")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST";
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Accept")
        request.httpBody = data;
        
        var IS_FINISN:Bool = false
        var IS_ERROR:Bool = false
        
        let task = session.dataTask(with: request, completionHandler: {
            (data,response,error)->Void in
            
            if (error != nil)
            {
                IS_ERROR = true
                IS_FINISN = true
            }
            else
            {
                let joRet:[String:AnyObject] = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:AnyObject]
                let state = (joRet["state"] as? NSNumber)?.boolValue
                if (state!){
                    self.ErrorMsg = ""
                }
                else{
                    self.ErrorMsg = (joRet["result"] as? String)!
                }
                IS_FINISN = true
            }
        })
        
        task.resume()
        while(!IS_FINISN) {
            RunLoop.current.run(until: Date())
        }
        
        return !IS_ERROR
    }
    
    private func getDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let st = dateFormatter.string(from: pickerDate.date)
        return st
    }
}
