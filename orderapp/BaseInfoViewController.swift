//
//  BaseInfoViewController.swift
//  orderapp
//
//  Created by VSNB on 2017/8/17.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit

class BaseInfoViewController: UIViewController {
    
//    @IBOutlet weak var imgInfo: UIImageView!
    @IBOutlet weak var lbInfoTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var web: UIWebView!
    
    private let ShowIcon:[String] = ["圖1-最近訂單.jpg", "圖2-查詢訂單.jpg", "圖3-快訊.jpg", "圖4-門市Q&A .jpg", "圖5-數位學習.jpg", "圖6-服務品質知識庫.jpg"]
    
    private let url_:[String] = [
        "http://orderapios.hongya88.com.tw/apstore/aps200_view_ios.aspx?guid=70f705ae-8c13-48c1-91fb-6dd65c13e687&dcat=F200",
        "http://orderapios.hongya88.com.tw/apstore/aps200_view_ios.aspx?guid=2774e913-f9c6-41c6-837b-aeb98bc64a5a&dcat=F300",
        "http://orderapios.hongya88.com.tw/apstore/aps200_view_ios.aspx?guid=e276d5cc-0520-4de0-86c1-7db2989bec0b&dcat=F400",
        "http://orderapios.hongya88.com.tw/apstore/aps200_view_ios.aspx?guid=a0b18ceb-64fa-45d7-ae73-54f299767d2d&dcat=F500"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setInfo(_ num: Int, _ txt:String) -> Void {
    //    imgInfo.image = UIImage(named: ShowIcon[num])
        let url = URL(string: url_[num - 2])
        //    print(String(describing: url))
        let requestObj = URLRequest(url: url!)
        web.loadRequest(requestObj)
        lbInfoTitle.text = txt
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
