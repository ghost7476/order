//
//  FormalViewController.swift
//  orderapp
//
//  Created by VSNB on 2017/8/17.
//  Copyright © 2017年 VSNB. All rights reserved.
//

import Foundation
import UIKit

import AdSupport

class FormalViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnRefresh: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
     //   let deviceID: String = (UIDevice.current.identifierForVendor?.uuidString)!
     //   print(deviceID)  //150D97C0-91CA-4C28-ADE5-75C976B858B1
        // B4322985-E9AB-446D-8541-DC70BC239B68
     //   let deviceID = UIDevice.current.identifierForVendor!.uuidString
     //   COMMON_Funtion.UDeviceString = deviceID
        COMMON_Funtion.UDeviceString = UUID()
        print(COMMON_Funtion.UDeviceString)
        
//        let url = URL(string: COMMON_Funtion.URL_String + COMMON_Funtion.UDeviceString)
        let url = URL(string: COMMON_Funtion.URL_String + COMMON_Funtion.Token )
        print(String(describing: url))
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
        
    }
  //  IDFA = C74BF30D-DE36-4E99-8C82-35A255E91468
  //  Vendor = B4322985-E9AB-446D-8541-DC70BC239B68
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func RefreshClick(_ sender: Any) {
        let url = URL(string: COMMON_Funtion.URL_String + COMMON_Temporary.getFCMToken()!)
        //    print(String(describing: url))
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
    }
    
    func UUID() -> String {
        var uu_id = "";
        var strIDFA : String = "No IDFA"
        if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
            strIDFA = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        }
        print("IDFA = \(strIDFA)")
        
        let strIDFV = UIDevice.current.identifierForVendor?.uuidString
        print("Vendor = \(strIDFV!)")
        
        if(strIDFA != "No IDFA"){
            uu_id = strIDFA;
        }
        else{
            uu_id = strIDFV!;
        }
        return uu_id
    }/**/
}
